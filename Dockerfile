FROM jenkins/jenkins:latest AS tlprjenkins
MAINTAINER TLPR
RUN apt-get update && apt-get install java -y && apt-get install maven -y && apt-get install ansible -y && apt-get install software-properties-common -y && add-apt-repository --yes --update ppa:ansible/ansible && apt-get install ansible -y
FROM tlprjenkins
USER root
EXPOSE 8080
CMD ["bash"]